Backend for http://jugendhacktlab.qdrei.info/nachricht/

## Dependencies

- any webserver capable doing CGIs
- `dash`, `sed`
- `make`
- `xsltproc`
- [`form2xhtml`](https://mro.name/form2xhtml), download from https://dev.mro.name/form2xhtml/

