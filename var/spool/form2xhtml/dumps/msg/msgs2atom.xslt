<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  exclude-result-prefixes="h"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/2005/Atom"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <feed xml:lang="de">
      <title>Gästebuch | Jugend Hackt Lab Traunstein</title>
      <id>http://jugendhacktlab.qdrei.info/nachricht/</id>
      <link rel="self" href="http://jugendhacktlab.qdrei.info/2022/gaestebuch/feed.xml"/>
      <link rel="hub" href="https://websubhub.com/hub"/>
      <updated><xsl:value-of select="$now"/></updated>
      <generator uri="https://codeberg.org/jugendhacktlab.qdrei.info" version="0.1">Jugend Hackt Lab TS</generator>
      <author><name>Bernd</name></author>
      <xsl:apply-templates select="h:html/h:body/h:ul/h:li[position() &lt; 51]">
        <xsl:sort select="@data-published" order="descending"/>
      </xsl:apply-templates>
    </feed>
  </xsl:template>

  <xsl:template match="h:li">
    <entry>
      <updated>
        <xsl:value-of select="@data-published"/>
      </updated>
      <id>http://jugendhacktlab.qdrei.info/2022/gaestebuch/#t<xsl:value-of select="@data-published"/></id>
      <link rel="self" href="http://jugendhacktlab.qdrei.info/2022/gaestebuch/#t{@data-published}"/>
      <title>Nachricht <xsl:call-template name="human_time_short">
          <xsl:with-param name="time" select="@data-published"/>
        </xsl:call-template>
      </title>
      <content>
        <xsl:for-each select="h:form/h:textarea[@name='msg']/text()">
          <xsl:copy/>
        </xsl:for-each>
      </content>
    </entry>
  </xsl:template>

  <xsl:template name="calculate-day-of-the-week">
    <!-- https://www.oreilly.com/library/view/xslt-cookbook/0596003722/ch03s02.html -->
    <xsl:param name="date-time"/>
    <xsl:param name="date" select="substring-before($date-time,'T')"/>
    <xsl:param name="year" select="substring-before($date,'-')"/>
    <xsl:param name="month" select="substring-before(substring-after($date,'-'),'-')"/>
    <xsl:param name="day" select="substring-after(substring-after($date,'-'),'-')"/>
    <xsl:variable name="a" select="floor((14 - $month) div 12)"/>
    <xsl:variable name="y" select="$year - $a"/>
    <xsl:variable name="m" select="$month + 12 * $a - 2"/>
    <xsl:value-of select="($day + $y + floor($y div 4) - floor($y div 100) + floor($y div 400) + floor((31 * $m) div    12)) mod 7"/>
  </xsl:template>

  <xsl:template name="human_time">
    <xsl:param name="time">-</xsl:param>
    <xsl:value-of select="substring($time, 9, 2)"/>
    <xsl:text>. </xsl:text>
    <xsl:variable name="month" select="substring($time, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="'01' = $month">Jan</xsl:when>
      <xsl:when test="'02' = $month">Feb</xsl:when>
      <xsl:when test="'03' = $month">Mar</xsl:when>
      <xsl:when test="'04' = $month">Apr</xsl:when>
      <xsl:when test="'05' = $month">May</xsl:when>
      <xsl:when test="'06' = $month">Jun</xsl:when>
      <xsl:when test="'07' = $month">Jul</xsl:when>
      <xsl:when test="'08' = $month">Aug</xsl:when>
      <xsl:when test="'09' = $month">Sep</xsl:when>
      <xsl:when test="'10' = $month">Oct</xsl:when>
      <xsl:when test="'11' = $month">Nov</xsl:when>
      <xsl:when test="'12' = $month">Dec</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 4)"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 12, 5)"/>
    <!-- xsl:text> Uhr</xsl:text -->
  </xsl:template>

  <xsl:template name="human_time_short">
    <xsl:param name="time">-</xsl:param>
    <xsl:variable name="wday">
      <xsl:call-template name="calculate-day-of-the-week">
        <xsl:with-param name="date-time" select="$time"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="substring($time, 12, 2)"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="substring($time, 15, 2)"/>
    <!-- xsl:text> Uhr</xsl:text -->
    <xsl:text>, </xsl:text>
    <xsl:choose>
      <xsl:when test="0 = $wday">So</xsl:when>
      <xsl:when test="1 = $wday">Mo</xsl:when>
      <xsl:when test="2 = $wday">Di</xsl:when>
      <xsl:when test="3 = $wday">Mi</xsl:when>
      <xsl:when test="4 = $wday">Do</xsl:when>
      <xsl:when test="5 = $wday">Fr</xsl:when>
      <xsl:when test="6 = $wday">Sa</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="substring($time, 9, 2)"/>
    <xsl:text>. </xsl:text>
    <xsl:variable name="month" select="substring($time, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="'01' = $month">Jan</xsl:when>
      <xsl:when test="'02' = $month">Feb</xsl:when>
      <xsl:when test="'03' = $month">Mär</xsl:when>
      <xsl:when test="'04' = $month">Apr</xsl:when>
      <xsl:when test="'05' = $month">Mai</xsl:when>
      <xsl:when test="'06' = $month">Jun</xsl:when>
      <xsl:when test="'07' = $month">Jul</xsl:when>
      <xsl:when test="'08' = $month">Aug</xsl:when>
      <xsl:when test="'09' = $month">Sep</xsl:when>
      <xsl:when test="'10' = $month">Okt</xsl:when>
      <xsl:when test="'11' = $month">Nov</xsl:when>
      <xsl:when test="'12' = $month">Dez</xsl:when>
      <xsl:when test="'12' = $month">Dez</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
