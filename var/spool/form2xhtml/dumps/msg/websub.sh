#!/bin/sh
# https://www.w3.org/TR/2018/REC-websub-20180123/
# http://pubsubhubbub.github.io/PubSubHubbub/pubsubhubbub-core-0.3.html#discovery
# e.g. https://github.com/pubsubhubbub/php-publisher/blob/master/library/Publisher.php
# https://websubhub.com

set -eu

HUB_URL="$(xmllint --xpath "string(/*/*[@rel='hub']/@href)" "${1}")"
TOPIC_URL="$(xmllint --xpath "string(/*/*[@rel='self']/@href)" "${1}")"

curl \
  --data "hub.mode=publish" \
  --data "hub.topic=${TOPIC_URL}" \
  --data "hub.url=${TOPIC_URL}" \
  --max-time 3 \
  --request POST \
  --silent \
  --url "${HUB_URL}" \
  --user-agent "https://codeberg.org/jugendhacktlab.qdrei.info/gaestebuch"

