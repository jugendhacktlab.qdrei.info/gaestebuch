#!/bin/sh
# move older 31 days from new to cur
#
# call me e.g. weekly via cron:
# $ sudo -u www-data crontab -l
# 55 11 * * Mon sh /var/spool/form2xhtml/dumps/msg/new2cur.sh

cd "$(dirname "${0}")" || exit 1

find "./new/" \
  -mtime +91 \
  -print \
| while read -r post
do
  bas="$(dirname "${post}")/$(basename "${post}" .post)"
  mv -- "${bas}".* "./cur/"
done

touch -t 198001020304 msgs.xml \
  && make atom.xml

