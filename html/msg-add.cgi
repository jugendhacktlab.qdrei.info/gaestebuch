#!/bin/sh
# dump HTTP post requests. Nice for off-site, static website feedback.
#
# Use e.g. https://mro.name/form2xhtml to process them.

readonly name="msg"

# where do you want to dump to?
# ensure existing maildir structure
cd "/var/spool/form2xhtml/dumps/${name}/new/" \
  && cd "../tmp/" \
  && cd ".." \
  || exit 1

# TODO ensure Makefile is r/o

# https://stackoverflow.com/a/52363117
[ $((CONTENT_LENGTH)) -gt 0 ] || exit 2

# upload size fuse
if [ $((CONTENT_LENGTH)) -gt 512 ] ; then
  cat <<EndOfMessage
Status: 413 Content Too Large

Content Too Large: $((CONTENT_LENGTH))
EndOfMessage
  exit 0
fi

# backlog count fuse
if [ $(($(ls ./new/????-??-??T??????_????.post | wc -l))) -gt 100 ] ; then
  cat <<EndOfMessage
Status: 503 Service Unavailable

Overflowing, traffic jam straight up ahead!
EndOfMessage
  exit 0
fi

readonly dst="$(date +%FT%H%M%S%z | sed -e 's/+/_/').post"
{
  printf "%s: %s\r\n" "Content-Type" "${CONTENT_TYPE}"
  printf "%s: %s\r\n" "Content-Length" "${CONTENT_LENGTH}"
  # printf "%s: \"%s\"\r\n" "User-Agent" "${HTTP_USER_AGENT}"
  printf "%s: %s\r\n" "Remote-Address" "${REMOTE_ADDR}"
  printf "\r\n"
  cat
} > "tmp/${dst}" \
 && chmod a-wx "tmp/${dst}" \
 && mv "tmp/${dst}" "new/" \
 && make --quiet websub

cat <<EndOfMessage
Status: 302 Found
Location: 2022/gaestebuch/

Danke!
EndOfMessage

