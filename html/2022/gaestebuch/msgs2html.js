
// https://code.mro.name/mro/ShaarliGo/src/master/static/themes/current/posts.js#L71
// make http, geo URIs (RFC 5870), RFC, ISBN, EAN and CVE clickable + microformat
function clickableTextLinks(elmsRendered) {
  // console.log('make http and geo URIs (RFC 5870) clickable + microformat');
  for (var i = elmsRendered.length - 1; i >= 0 ; i--) {
    const elm = elmsRendered[i];
    elm.innerHTML = elm.innerHTML
      .replace(/(https?:\/\/[^ \t\r\n"']+[^ ?\t\r\n"'.,;()])/gi, '<a rel="noreferrer" class="http" href="$1">$1</a>')
    // https://alanstorm.com/url_regex_explained/ \b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))
    // .replace(/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/gi, '<a rel="noreferrer" class="http" href="$1">$1</a>')
      .replace(/(geo:(-?\d+.\d+),(-?\d+.\d+)(\?z=(\d+))?)/gi, '<a class="geo" href="https://www.openstreetmap.org/?mlat=$2&amp;mlon=$3" title="zoom=$5">geo:<span class="latitude">$2</span>,<span class="longitude">$3</span>$4</a>')
      .replace(/(urn:ietf:rfc:(\d+)(#\S*[0-9a-z])?)/gi, '<a class="rfc" href="https://tools.ietf.org/html/rfc$2$3" title="RFC $2">$1</a>')
      .replace(/(urn:isbn:([0-9-]+)(#\S*[0-9a-z])?)/gi, '<a class="isbn" href="https://de.wikipedia.org/wiki/Spezial:ISBN-Suche?isbn=$2" title="ISBN $2">$1</a>')
      .replace(/(urn:ean:([0-9-]+)(#\S*[0-9a-z])?)/gi, '<a class="ean" href="https://www.ean-suche.de/?q=$2" title="EAN $2">$1</a>')
      .replace(/(CVE-[0-9-]+-[0-9]+)/gi, '<a class="cve" href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=$1">$1</a>');
  }
}

clickableTextLinks(document.getElementById('msgs').getElementsByTagName('pre'));

document.documentElement.className = "#waskommthintenhin" === location.hash
  ? "admin"
  : "";


document.getElementById("add").onsubmit = (evt) => {
  let o = evt.currentTarget.submit;
  o.value = "⏳ sending …";
  o.disabled = true;
  true;
};

