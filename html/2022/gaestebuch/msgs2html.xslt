<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  exclude-result-prefixes="h"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
    <xsl:output
      encoding="utf-8"
      method="xml"
      omit-xml-declaration="yes"
      indent="yes"/>

  <xsl:template match="/">
    <xsl:variable name="is_open" select="1 = count(document('./open.xml')/open)"/>
    <html xml:lang="de" lang="de" class="replaced by msg2html.js">
      <head>
        <link href="../style.css" rel="stylesheet" type="text/css"/>
        <link rel="alternate" type="application/atom+xml" href="http://jugendhacktlab.qdrei.info/2022/gaestebuch/feed.xml"/>
        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <meta http-equiv="refresh" content="303"/>
        <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"/>
        <meta name="keywords" content="Jugend Hackt Lab Traunstein Gästebuch Feedback"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <title>Gästebuch | Jugend Hackt Lab TS</title>
      </head>
      <body>
        <xsl:if test="$is_open">
          <xsl:attribute name="class">open</xsl:attribute>
        </xsl:if>
        <h1>Gästebuch | Jugend Hackt Lab TS</h1>
	<p>🕙 Dieser Server ist von 22:00 bis 7:00 Uhr aus. Da ist Schlafenszeit. 🛌
	</p>
        <!-- p>
          <a href=".">Neue Nachricht schreiben</a>
        </p -->
        <p id="tuere">
          <img id="zu"  alt="Türe" src="tuere-zu.png"  title="Türe zu: niemand da"/>
          <img id="auf" alt="Türe" src="tuere-auf.png" title="Türe offen!"/>
        </p>
        <form name="add" id="add" action="../../msg-add.cgi" method="post" enctype="multipart/form-data">
          <textarea placeholder="Deine Nachricht in's Lab! &#10;&#10;Der Text wird dann hier unten und auf einem &#10;Display im Jugend Hackt Lab angezeigt." name="msg" autofocus="autofocus"/>
          <input type="submit" name="submit"/>
        </form>
        <ol id="msgs">
          <xsl:for-each select="h:html/h:body/h:ul/h:li">
            <xsl:sort select="@data-published" order="descending"/>
            <li id="t{@data-published}">
              <a href="#t{@data-published}" data-published="{@data-published}">
                <!-- xsl:value-of select="substring(translate(@data-published, 'T', ' '), 1, 15)"/ -->
                <xsl:call-template name="human_time_short">
                  <xsl:with-param name="time" select="@data-published"/>
                </xsl:call-template>
              </a>
              <pre>
                <xsl:for-each select="h:form/h:textarea[@name='msg']/text()">
                  <xsl:copy></xsl:copy>
                </xsl:for-each>
              </pre>
              <form name="delete" action="../../msg-delete.cgi?{@data-published}" method="post">
                <input type="submit" value="Löschen"/>
              </form>
            </li>
          </xsl:for-each>
        </ol>
        <p id="footer">
          <a title="Abonnieren" href="feed.xml">
            <img alt="Feed" src="feed-icon.svg" class="icon"/>
          </a>
          <xsl:text> </xsl:text>
          <a title="Validate (Atom 1.0)" href="https://validator.w3.org/feed/check.cgi?url=http://jugendhacktlab.qdrei.info/2022/gaestebuch/feed.xml">
            <img alt="Validity badge (Atom 1.0)" src="valid-atom.svg" class="icon"/>
          </a>
          <xsl:text> </xsl:text>
          <a rel="terms-of-service" href="/impressum/">§ Impressum</a>
          <xsl:text> * </xsl:text>
          <a rel="privacy-policy" href="/datenschutz/">§ Datenschutz</a>
          <xsl:text> * </xsl:text>
          <a rel="version-history" href="/codeberg/">🛠 Quelltext</a>
        </p>
        <script src="./msgs2html.js"></script>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="calculate-day-of-the-week">
    <!-- https://www.oreilly.com/library/view/xslt-cookbook/0596003722/ch03s02.html -->
    <xsl:param name="date-time"/>
    <xsl:param name="date" select="substring-before($date-time,'T')"/>
    <xsl:param name="year" select="substring-before($date,'-')"/>
    <xsl:param name="month" select="substring-before(substring-after($date,'-'),'-')"/>
    <xsl:param name="day" select="substring-after(substring-after($date,'-'),'-')"/>
    <xsl:variable name="a" select="floor((14 - $month) div 12)"/>
    <xsl:variable name="y" select="$year - $a"/>
    <xsl:variable name="m" select="$month + 12 * $a - 2"/>
    <xsl:value-of select="($day + $y + floor($y div 4) - floor($y div 100) + floor($y div 400) + floor((31 * $m) div 12)) mod 7"/>
  </xsl:template>

  <xsl:template name="human_time">
    <xsl:param name="time">-</xsl:param>
    <xsl:value-of select="substring($time, 9, 2)"/>
    <xsl:text>. </xsl:text>
    <xsl:variable name="month" select="substring($time, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="'01' = $month">Jan</xsl:when>
      <xsl:when test="'02' = $month">Feb</xsl:when>
      <xsl:when test="'03' = $month">Mar</xsl:when>
      <xsl:when test="'04' = $month">Apr</xsl:when>
      <xsl:when test="'05' = $month">May</xsl:when>
      <xsl:when test="'06' = $month">Jun</xsl:when>
      <xsl:when test="'07' = $month">Jul</xsl:when>
      <xsl:when test="'08' = $month">Aug</xsl:when>
      <xsl:when test="'09' = $month">Sep</xsl:when>
      <xsl:when test="'10' = $month">Oct</xsl:when>
      <xsl:when test="'11' = $month">Nov</xsl:when>
      <xsl:when test="'12' = $month">Dec</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 4)"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 12, 5)"/>
    <!-- xsl:text> Uhr</xsl:text -->
  </xsl:template>

  <xsl:template name="human_time_short">
    <xsl:param name="time">-</xsl:param>
    <xsl:variable name="wday">
      <xsl:call-template name="calculate-day-of-the-week">
        <xsl:with-param name="date-time" select="$time"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="substring($time, 12, 2)"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="substring($time, 15, 2)"/>
    <!-- xsl:text> Uhr</xsl:text -->
    <xsl:text>, </xsl:text>
    <xsl:choose>
      <xsl:when test="0 = $wday">So</xsl:when>
      <xsl:when test="1 = $wday">Mo</xsl:when>
      <xsl:when test="2 = $wday">Di</xsl:when>
      <xsl:when test="3 = $wday">Mi</xsl:when>
      <xsl:when test="4 = $wday">Do</xsl:when>
      <xsl:when test="5 = $wday">Fr</xsl:when>
      <xsl:when test="6 = $wday">Sa</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="substring($time, 9, 2)"/>
    <xsl:text>. </xsl:text>
    <xsl:variable name="month" select="substring($time, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="'01' = $month">Jan</xsl:when>
      <xsl:when test="'02' = $month">Feb</xsl:when>
      <xsl:when test="'03' = $month">Mär</xsl:when>
      <xsl:when test="'04' = $month">Apr</xsl:when>
      <xsl:when test="'05' = $month">Mai</xsl:when>
      <xsl:when test="'06' = $month">Jun</xsl:when>
      <xsl:when test="'07' = $month">Jul</xsl:when>
      <xsl:when test="'08' = $month">Aug</xsl:when>
      <xsl:when test="'09' = $month">Sep</xsl:when>
      <xsl:when test="'10' = $month">Okt</xsl:when>
      <xsl:when test="'11' = $month">Nov</xsl:when>
      <xsl:when test="'12' = $month">Dez</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
