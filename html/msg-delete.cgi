#!/bin/sh

readonly name="msg"
cd "/var/spool/form2xhtml/dumps/${name}/" || exit 1

file="$(echo "$(basename "${QUERY_STRING}")" | sed 's/+/_/;s/://g')"

cat <<EOF
Status: 302 Found
Location: ./2022/gaestebuch/

EOF

mv -- "./new/${file}".* ./cur/ \
&& make --quiet -B atom.xml

