#!/bin/sh
cd "$(dirname "${0}")" || exit 1

rsync -avPz -e "ssh -p 2002" jugendhacktlab.qdrei.info:/var/www/html/*.cgi html/
rsync -avPz -e "ssh -p 2002" jugendhacktlab.qdrei.info:/var/www/html/2022/style.css html/2022/
rsync -avPz -e "ssh -p 2002" jugendhacktlab.qdrei.info:/var/www/html/2022/gaestebuch html/2022/
rsync -avPz -e "ssh -p 2002" jugendhacktlab.qdrei.info:/var/spool/form2xhtml/dumps/msg var/spool/form2xhtml/dumps/

